#!/usr/bin/python
import mysql.connector
import imp
import sys
import os.path
import urllib
import creds.awsAndBuckets
import creds.sandboxConfig
import threadder

try:
	imp.find_module('boto')
except ImportError:
	print 'boto has not been found!'
	sys.exit(1)

try:
    imp.find_module('mysql')
except ImportError:
    print 'mysql.connector module not found. Download and install it from http://dev.mysql.com/downloads/file/?id=458964'
    sys.exit(1)

try:
    imp.find_module('prettytable')
except ImportError:
    print 'PrettyTable module not found. Install it by running "sudo pip install prettytable" (install pip using "sudo easy_install pip" if needed)'
    sys.exit(1)

try:
	imp.find_module('zipfile')
except ImportError:
	print 'zipfile has not been found! use *sudo pip install zipfile*'
	sys.exit(1)

from prettytable import PrettyTable
from tenant import Tenant
from boto.s3.connection import S3Connection


#imports from own scripts
from localdatabaseManipulator import getTenants
from threadder import splitAllTenantIntoFourEqualTenants


def start():
	#all tenants to be downloaded!
	tenants = []
	#all threads runing
	threads = []
	tenants = getTenants()
	threads = splitAllTenantIntoFourEqualTenants(tenants)
	threadder.downloadTenants(threads)
	print '####################'
	print threads

start()
