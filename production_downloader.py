#!/usr/bin/python
import creds.awsAndBuckets
import os
import localdatabaseManipulator as ldbm
import zipper
import time

from datetime import datetime



from boto.s3.connection import S3Connection

LOCAL_PATH = os.path.dirname(os.path.realpath(__file__))+"/dumps/"

def connectAndDownload(Tenant):
	#Download dump from S3 production firstly
	if Tenant.option == 'SKIP':
		print 'Skipping tenant %s due to option SKIP' % (Tenant.tenantName)
	elif Tenant.option == 'OBFUSCATE':
		print 'Obfuscating tenant %s and setting option to SKIP. Please do not forget to remove this option!' % (Tenant.tenantName)
	elif Tenant.state != 'DONE' and Tenant.state:
		print 'An import is already running for tenant %s' % (Tenant.tenantName)
	else: 
		conn = S3Connection(creds.awsAndBuckets.AWS_ACCESS_KEY_ID, creds.awsAndBuckets.AWS_SECRET_ACCESS_KEY)
		
		if Tenant.location == 'EUW':
			bucket = conn.get_bucket(creds.awsAndBuckets.BUCKET_EUW)
		elif Tenant.location == 'USE':
			bucket = conn.get_bucket(creds.awsAndBuckets.BUCKET_USE)

		allKeys = [(k.last_modified, k) for k in bucket.list(prefix=Tenant.tenantName + '/'+Tenant.tenantName+'-BACKUP')]
		if allKeys:
			key_to_download = sorted(allKeys, cmp=lambda x,y: cmp(x[0], y[0]))[-1][1]
			S3DumpAge = key_to_download.last_modified
			S3DumpAge = S3DumpAge.split('T')
			dumpAge = S3DumpAge[0]
			localDatabaseAge = Tenant.BackupDate
			try:
				if Tenant.option == 'FORCE':
					print 'Forcefully downloading tenant %s due to tenant option %s' % (Tenant.tenantName, Tenant.option)
					ldbm.setOption(Tenant,'')
					download(key_to_download,Tenant,dumpAge)
				elif time.strptime(dumpAge,"%Y-%m-%d") > time.strptime(localDatabaseAge,"%Y-%m-%d"):
					download(key_to_download,Tenant,dumpAge)
				else:
					print "Skipping %s since the dump has the same date as the local one!( Dump: %s , Local: %s )" % (Tenant.tenantName,dumpAge,Tenant.BackupDate)
			except (ValueError,TypeError) as e:
				print "Tenant %s is new or it has an invalid date!" % (Tenant.tenantName)
				download(key_to_download,Tenant,dumpAge)





def download(key_to_download,Tenant,dumpAge):
	filename = key_to_download.name.split('/')
	filepath = LOCAL_PATH + 's3/'  + filename[1]
	print filepath
	print "Downloading %s from %s production servers" % (Tenant.tenantName, Tenant.location)
	ldbm.setState(Tenant,"DOWNLOADING")
	ldbm.setCurentBackup(Tenant,filename[1],dumpAge)
	key_to_download.get_contents_to_filename(filepath)
	unzippedFilePath = LOCAL_PATH + "production/"
	ldbm.setState(Tenant,"UNZIPING")
	zipper.unzipFile(filepath,unzippedFilePath,Tenant.tenantName)
	#Remove the zip file since it's not needed anymore!
	os.system("rm %s" % (filepath))
	ldbm.setState(Tenant,"IMPORTING")
	dump_filename = LOCAL_PATH + "production/"+ Tenant.tenantName + ".sql"
	ldbm.importTenant(Tenant.tenantName,Tenant.type,dump_filename)
	ldbm.setState(Tenant,"DONE")
	os.system("rm %s" % (dump_filename))
	print "Finished importing %s_%s" %(Tenant.tenantName,Tenant.type)