#!/usr/bin/python
class Tenant:
	def __init__(self,tenantName,priority,location,type,currentBackup,BackupDate,state,option):
		self.tenantName = tenantName
		self.priority = priority
		self.location = location
		self.type = type
		self.currentBackup = currentBackup
		self.BackupDate = BackupDate
		self.state = state
		self.option = option
	def __repr__(self):
		return repr((self.tenantName, self.priority, self.location, self.type,self.currentBackup,self.BackupDate,self.state,self.option))


#tenant, priority, location, type, currentBackup, BackupDate, state, option