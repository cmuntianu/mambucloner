#!/usr/bin/python
import numpy as np
import multiprocessing
import production_downloader
import sandbox_downloader

# Used to create 4 separate arrays so we can download 4 tenants at a time 
def splitAllTenantIntoFourEqualTenants(tenant_objects):
	count = 0;
	#All threads
	threads= []
	#Stores Tenants downloaded by thread 1
	threaded_tenants_1 = []
	#Stores Tenants downloaded by thread 2
	threaded_tenants_2 = []
	#Stores Tenants downloaded by thread 3
	threaded_tenants_3 = []
	#Stores Tenants downloaded by thread 4
	threaded_tenants_4 = []
	for tenant in tenant_objects:
		if count%4 == 0:
			threaded_tenants_1.append(tenant)
		elif count % 4 == 1:
			threaded_tenants_2.append(tenant)
		elif count % 4 == 2:
			threaded_tenants_3.append(tenant)
		elif count % 4 == 3:
			threaded_tenants_4.append(tenant)
		count = count + 1
	threads.append(threaded_tenants_1)
	threads.append(threaded_tenants_2)
	threads.append(threaded_tenants_3)
	threads.append(threaded_tenants_4)
	return threads

#Download a Tenant
def donwloadTenant(Tenant,threadId):
	print 'Downloading tenant ' + Tenant.tenantName + ' using thread: '+ str(threadId) 
	if Tenant.type == 'production':
		production_downloader.connectAndDownload(Tenant)
	elif Tenant.type == 'sandbox':
		sandbox_downloader.connectAndDownload(Tenant)


	

#method called within a thread to download its asigned tenants
def threadDownload(thread_tenant,threadId):
	for tenant in thread_tenant:
		donwloadTenant(tenant,threadId)




#create a min of 4 threads and download each tenant on a separate thread
def downloadTenants(threads):
	
		processThread1 = multiprocessing.Process(target=threadDownload, args=(threads[0],1))
		processThread2 = multiprocessing.Process(target=threadDownload, args=(threads[1],2))
		processThread3 = multiprocessing.Process(target=threadDownload, args=(threads[2],3))
		processThread4 = multiprocessing.Process(target=threadDownload, args=(threads[3],4))
		processThread1.start()
		processThread2.start()
		processThread3.start()
		processThread4.start()

		processThread1.join()
		processThread2.join()
		processThread3.join()
		processThread4.join()