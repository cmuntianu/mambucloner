# Obfuscation script that tries to anonymize any data of a production database that could
# lead to information on a person or tenant

# Note that this script was created based on a sample production database and does not
# guarantee that all private information is removed for any production database since
# customers tend to put private information in any column that you would not expect
# (e.g. plain text passwords in notes columns)

# Note that this script needs to be updated when a database change occurs

update activity set notes = null;

delete from address;

#delete from backgroundprocess;

update branch set	name = substring(cast(md5(name) as char),1,5),
					phonenumber = null, 
					emailaddress = null, 
					notes = null,
					phonenumber=null, 
					emailaddress=null;

update centre set 	name = substring(cast(md5(name) as char),1,5), 
					notes = null;

update client set 	firstname =  substring(cast(md5(firstname) as char),1,5), 
					lastname =  substring(cast(md5(lastname) as char),1,5), 
					middlename =  substring(cast(md5(middlename) as char),1,5), 
					emailaddress = null, 
					homephone =  null, 
					profilepicturekey = null, 
					profilesignaturekey = null, 
					mobilephone1 = null, 
					mobilephone2 =  null, 
					notes = null;


delete from comment;

 

delete from customreport;
# alternatively: update customreport set `name`=substring(cast(md5(`name`) as char),1,5), `description`=substring(cast(md5(`description`) as char),1,5);

# overwrite any currency with a default one to obfuscate the country
update `currency` set `CODE`='GHS', `NAME`='Ghana cedi', `SYMBOL`='GH₵';

update databasechangelog set tag=null, comments=null;

delete from document;

delete from fieldchangeitem;

update glaccount set 	`name`=substring(cast(md5(`name`) as char),1,5),
						`description`=substring(cast(md5(`description`) as char),1,5);

update gljournalentry set `notes`=substring(cast(md5(`notes`) as char),1,5);

update `group` set 	notes = null, 
					emailaddress = null, 
					homephone =  null, 
					mobilephone1 =  null,
					groupname =  substring(cast(md5(groupname) as char),1,5);

update guaranty set `assetname`=substring(cast(md5(`assetname`) as char),1,5);

update holiday set name=substring(cast(md5(name) as char),1,7);

delete from identificationdocument;

update identificationdocumenttemplate set mandatoryforclients = 0;

delete from image;

update loanaccount set 	`notes`=substring(cast(md5(`notes`) as char),1,5),
						`loanname`=substring(cast(md5(`loanname`) as char),1,5) ;

update loanproduct set 	`productdescription`=substring(cast(md5(`productdescription`) as char),1,5),
						`productname`=substring(cast(md5(`productname`) as char),1,5);

update loantransaction set `comment`=substring(cast(md5(`comment`) as char),1,5);

delete from messagetemplate;


truncate table notificationrequest;



update `organization` set 	`TIMEZONEID`='Africa/Accra',
							emailaddress = null,
							phoneno = null,
							`name` = substring(cast(md5(`name`) as char),1,5);

delete from organizationbranding;

update repayment set `notes`=substring(cast(md5(`notes`) as char),1,5);

update savingsaccount set 	`notes`=substring(cast(md5(`notes`) as char),1,5), 
							name=substring(cast(md5(name) as char),1,7);

update savingsproduct set 	`description`=substring(cast(md5(`description`) as char),1,5),
							`name`=substring(cast(md5(`name`) as char),1,5);
                            
                            

update savingstransaction set `comment`=substring(cast(md5(`comment`) as char),1,5);

update task set 	`description`=substring(cast(md5(`description`) as char),1,5),
					`title`=substring(cast(md5(`title`) as char),1,5);

update transactiondetails set 	`banknumber`=substring(cast(md5(`banknumber`) as char),1,5),
								`checknumber`=substring(cast(md5(`checknumber`) as char),1,5),
								`receiptnumber`=substring(cast(md5(`receiptnumber`) as char),1,5),
								`routingnumber`=substring(cast(md5(`routingnumber`) as char),1,5);

#set all passwords to cloudy2010
update user set 	email = null, 
					homephone = null, 
					mobilephone1 = null, 
					notes = null, 
					firstname = substring(cast(md5(firstname) as char),1,5),
					lastname = substring(cast(md5(lastname) as char),1,5),
					title=substring(cast(md5(title) as char),1,7),
					`PASSWORD`='$2a$05$bPc0grhQkOGx.bJ9VqzeMO3drDX3FHKh/8gvKn0oEsNy6kagGWuCS' ;