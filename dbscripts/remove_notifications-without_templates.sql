update branch set	phonenumber = null, 
					emailaddress = null;

update client set 	emailaddress = null, 
					homephone =  null, 
					mobilephone1 = null, 
					mobilephone2 =  null;

update `group` set 	emailaddress = null, 
					homephone =  null, 
					mobilephone1 =  null;

truncate table notificationmessage;
truncate table notificationrequest;

update webhooknotificationsettings set 	notificationstate = 'DISABLED' ;
update emailnotificationsettings 
set EMAILCREDENTIALSPROVIDER = 'NONE',
    `FROMEMAIL` = NULL,
    `FROMNAME` = NULL,
    `PASSWORD` = NULL,
    `REPLAYTOEMAIL` = NULL,
    `SMTPHOST` = NULL,
    `SMTPPORT` = NULL,
    `USERNAME` = NULL;

update smsnotificationsettings set 	gateway = 'NONE',
									password = null,
									username = null,
									fromnumber = null,
									lastmodifieddate = null;

update `organization` set   emailaddress = null,
							phoneno = null;

update user set 	email = null, 
					homephone = null, 
					mobilephone1 = null ;
 			