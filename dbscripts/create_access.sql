UPDATE user SET username='demo' where isadministrator=1 and issupport = 0 limit 1;
UPDATE user SET PASSWORD='\$2a\$05\$bPc0grhQkOGx.bJ9VqzeMO3drDX3FHKh/8gvKn0oEsNy6kagGWuCS';
UPDATE user SET language='ENGLISH' where username = 'demo';
update passwordresetrequest set state='COMPLETED';
update user set TWOFACTORAUTHENTICATION=0;