#!/usr/bin/python
import paramiko

def createSSHConnection(config):
	key = paramiko.RSAKey.from_private_key_file(config["ssl_key"])
	SshConnection = paramiko.SSHClient()
	SshConnection.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	SshConnection.load_system_host_keys()
	SshConnection.connect(config["bastion_host"], username = "ec2-user", pkey = key)
	return SshConnection