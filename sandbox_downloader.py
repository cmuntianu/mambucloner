#!/usr/bin/python
import creds.sandboxConfig as sandboxConfig
import sslConnector
import paramiko
import os
import zipper
import localdatabaseManipulator as ldbm
import time

from time import gmtime,strftime
from datetime import datetime

LOCAL_PATH = os.path.dirname(os.path.realpath(__file__))+"/dumps/"


def connectAndDownload(Tenant):

	if Tenant.option == 'SKIP':
		print 'Skipping tenant %s due to option SKIP' % (Tenant.tenantName)
	elif Tenant.option == 'OBFUSCATE':
		print 'Obfuscating tenant %s and setting option to SKIP. Please do not forget to remove this option!' % (Tenant.tenantName)
		ldbm.obfuscate(Tenant.tenantName,Tenant.type)
		ldbm.setOption(Tenant,'SKIP')
		ldbm.setState(Tenant,'OBFUSCATED')
	elif Tenant.state != 'DONE' and Tenant.state:
		print 'An import is already running for tenant %s' % (Tenant.tenantName)
	elif Tenant.option == 'FORCE':
		print 'Forcefully downloading tenant %s due to tenant option %s' % (Tenant.tenantName, Tenant.option)
		ldbm.setOption(Tenant,'')
		connectAndDownloadIfPassedValidation(Tenant)
	elif not Tenant.currentBackup:
		connectAndDownloadIfPassedValidation(Tenant)
	else:
		backupTime = time.strptime(Tenant.currentBackup,"%Y-%m-%d")
		if backupTime != time.strptime(strftime("%Y-%m-%d",gmtime()),"%Y-%m-%d"):
			connectAndDownloadIfPassedValidation(Tenant)
		else:
			print 'Tenant %s from %s is already up to date(%s)!' % (Tenant.tenantName,Tenant.location,Tenant.currentBackup)



def connectAndDownloadIfPassedValidation(Tenant):
		if Tenant.location == 'EUW':
			config_sandbox = sandboxConfig.config_sandbox_EUW
		elif Tenant.location == 'USE':
			config_sandbox = sandboxConfig.config_sandbox_USE
			
		SSHConnection = sslConnector.createSSHConnection(config_sandbox)
		if SSHConnection:
			print "Secure Connection established on: %s , for downloading %s " % (config_sandbox["name"], Tenant.tenantName)
			decompressedFilePath = createDump(Tenant,config_sandbox,SSHConnection)
			ldbm.setState(Tenant,"IMPORTING")
			ldbm.importTenant(Tenant.tenantName,"sandbox",decompressedFilePath)
			print "Finished importing %s_%s" %(Tenant.tenantName,Tenant.type)
			ldbm.setState(Tenant,"DONE")
			pythonTime = datetime.now()
			backupDate = '%s-%s-%s' %(pythonTime.year,pythonTime.month,pythonTime.day)
			importDate = '%s-%s-%s %s:%s' %(pythonTime.year,pythonTime.month,pythonTime.day, pythonTime.hour, pythonTime.minute)
			ldbm.setCurentBackup(Tenant,backupDate,importDate)
			os.system("rm %s" % (decompressedFilePath))


def createDump(Tenant,config_sandbox,SshConnection):
	       command = 'mysqldump -u%s -p%s -h %s -P3306 --skip-lock-tables --ssl-ca=/home/ec2-user/rds-combined-ca-bundle.pem %s | gzip > /home/ec2-user/dumps/%s_sandbox' % (config_sandbox["user"], config_sandbox["password"], config_sandbox["host"], Tenant.tenantName, Tenant.tenantName)
	       print command
	       print "Creating dump on ec2 for tenant %s" % Tenant.tenantName
	       ldbm.setState(Tenant,"DUMPING")
	       stdin, stdout, stderr = SshConnection.exec_command(command)
	       while not stdout.channel.exit_status_ready():
	       	if stdout.channel.recv_ready():
	       		rl, wl, xl = select.select([stdout.channel], [], [], 0.0)
	       		if len(rl) > 0:
	       			print stdout.channel.recv(1024)
	       print "Successfully created dump on ec2 for tenant %s" % Tenant.tenantName
	       remoteFilePath='/home/ec2-user/dumps/%s_sandbox' % Tenant.tenantName
	       localFilePath= LOCAL_PATH + 'sandboxArchive/%s_sandbox' % Tenant.tenantName
	       print "Transferring file from ec2 to local for tenant %s" % Tenant.tenantName
	       sftp = SshConnection.open_sftp()
	       ldbm.setState(Tenant,"TRANSFERING")
	       sftp.get(remoteFilePath,localFilePath)
	       print "Finished transfering tenant %s" % Tenant.tenantName
	       command = 'rm %s' % (remoteFilePath)
	       stdin, stdout, stderr = SshConnection.exec_command(command)
	       decompressedFilePath = LOCAL_PATH + "sandbox/" + Tenant.tenantName + ".sql"
	       ldbm.setState(Tenant,"DECOMPRESSING")
	       zipper.decompressFile(localFilePath,decompressedFilePath,Tenant.tenantName)
	       os.system("rm %s" % (localFilePath))
	       return decompressedFilePath