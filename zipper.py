#!/usr/bin/python
import zipfile
import gzip

def unzipFile(zippedFilePath,unzippedFilePath, tenantName):
	print "Unzipping tenant " + tenantName
	with zipfile.ZipFile(zippedFilePath, "r") as z:
    		z.extractall(unzippedFilePath)
    	print "Finished Unzipping tenant " + tenantName

def decompressFile(compressedFilePath, decompressedFilePath, tenantName):
	print "Decompressing tenant " + tenantName
	with gzip.open(compressedFilePath,'rb') as decompressedFile:
		with open(decompressedFilePath, "wb") as f:
	    		 f.write(decompressedFile.read())
