#!/usr/bin/python
import mysql.connector
import boto
import subprocess
import creds.localConfig as localConfiguration
import os

from prettytable import PrettyTable
from tenant import Tenant
from subprocess import Popen,PIPE

tenant_objects = []

config_local = localConfiguration.config_local

scripts_Location = os.path.dirname(os.path.realpath(__file__))+"/dbscripts/"

# Prints the results in a table format
def printTable( cursor, results):
	table = PrettyTable(cursor.column_names)
	for item in results:
		table.add_row(item)
	print table

#Parses the Results
def parseResult(cursor,results):
	for item in results:
		tenant = Tenant(item[0],item[1],item[2],item[3],item[4],item[5],item[6],item[7])
		tenant_objects.append(tenant)
		print tenant
	tenant_objects.sort(key=lambda Tenant: Tenant.priority, reverse=False)
	return tenant_objects

#Gets all tenant details from local env
def getTenants():
	cnx = mysql.connector.connect(**config_local)
	try:
		cursor = cnx.cursor()
		query = 'select * from downloadlist;';
		cursor.execute(query)
		results = cursor.fetchall()
		printTable(cursor, results)
	finally:
		cnx.close()
	return parseResult(cursor,results)

#Sets the state for the specified tenant
def setState(Tenant, curentState):
	cnx = mysql.connector.connect(**config_local)
	try:
		cursor = cnx.cursor()
		cursor.execute ("""
			   UPDATE downloadlist
			   SET STATE=%s
			   WHERE tenant=%s and priority=%s and location=%s and type=%s
			""", (curentState,Tenant.tenantName,Tenant.priority,Tenant.location,Tenant.type))
		cnx.commit()
	finally:
		cnx.close()


#Sets the current backup name and the curent backupdate
def setCurentBackup(Tenant, curentBackup, backupDate):
	cnx = mysql.connector.connect(**config_local)
	try:
		cursor = cnx.cursor()
		cursor.execute ("""
			   UPDATE downloadlist
			   SET CURRENTBACKUP=%s , BACKUPDATE=%s
			   WHERE tenant=%s and priority=%s and location=%s and type=%s
			""", (curentBackup,backupDate,Tenant.tenantName,Tenant.priority,Tenant.location,Tenant.type))
		cnx.commit()
	finally:
		cnx.close()

def createSchemaForTenant(schema_name):
	cnx = mysql.connector.connect(**config_local)
	try:
		cursor = cnx.cursor()
		query = "DROP SCHEMA IF EXISTS " + schema_name + ";"
		cursor.execute(query)
		query = "CREATE DATABASE " + schema_name + ";"
		cursor.execute(query)
		cnx.commit()
	finally:
		cnx.close()


def obfuscate(tenantName,schemaType):
	command = 'mysql -u%s -p%s -h %s %s -B -N < %s' % (config_local["user"],config_local["password"],config_local["host"],tenantName+'_'+schemaType,scripts_Location+"obfuscate_all.sql")
	os.system(command)

def removeNotifications(tenantName,schemaType):
	print scripts_Location+"remove_notifications.sql"
	command = 'mysql -u%s -p%s -h %s %s -B -N < %s' % (config_local["user"],config_local["password"],config_local["host"],tenantName+'_'+schemaType,scripts_Location+"remove_notifications.sql")
	os.system(command)

def createAccess(tenantName,schemaType):
	command = 'mysql -u%s -p%s -h %s %s -B -N < %s' % (config_local["user"],config_local["password"],config_local["host"],tenantName+'_'+schemaType,scripts_Location+"create_access.sql")
	os.system(command)


def importTenant(tenantName,schemaType,dump_filename):
	schema_name = tenantName + "_" + schemaType
	createSchemaForTenant(schema_name)
	with open(dump_filename, 'r') as f: 
	       command = ['mysql', '-u%s' % config_local["user"], '-p%s' % config_local["password"], tenantName + "_" + schemaType]
	       proc = subprocess.Popen(command, stdin = f)
	       stdout, stderr = proc.communicate()
	print 'Removing notifications for tenant %s' % (tenantName + '_'+ schemaType)
	removeNotifications(tenantName,schemaType)
	print 'Creating Access for tenant %s' % (tenantName + '_'+ schemaType)
	createAccess(tenantName,schemaType)


#Sets the current backup name and the curent backupdate
def setOption(Tenant,option):
	cnx = mysql.connector.connect(**config_local)
	try:
		cursor = cnx.cursor()
		cursor.execute ("""
			   UPDATE downloadlist
			   SET `option`=%s
			   WHERE tenant=%s and priority=%s and location=%s and type=%s
			""", (option,Tenant.tenantName,Tenant.priority,Tenant.location,Tenant.type))
		cnx.commit()
	finally:
		cnx.close()


